local generate = function ()

    local colors = {}
    colors = {
        vscNone = 'NONE',
        vscFront = '#000000',
        vscBack = '#ffffea',

        vscTabCurrent = '#eaffff',
        vscTabOther = '#eecccc',
        vscTabOutside = '#eaeaea',

        vscLeftDark = '#eaffff',
        vscLeftMid = '#cceeff',
        vscLeftLight = '#eaeaea',
        vscLeftFront = '#000000',

        vscIndent = '#dddd93',
        vscIndentActive = '#cccc88',

        vscPopupFront = '#000000',
        vscPopupBack = '#eaffff',
        vscPopupHighlightBlue = '#9eeeee',
        vscPopupHighlightFront = '#000000',
        vscPopupHighlightGray = '#994c4c',

        vscSplitDark = '#eaeaea',

        vscCursorDarkDark = '#eeeecc',
        vscCursorDark = '#f5f5cc',
        vscCursorLight = '#ffffea',
        vscSelection = '#eeee9e',
        vscLineNumber = '#000000',

        vscDiffRedDark = '#ffeaea',
        vscDiffRedMid = '#ffaaaa',
        vscDiffRedLight = '#bb5d5d',
        vscDiffGreenDark = '#eaffea',
        vscDiffGreenMid = '#448844',
        vscDiffGreenLight = '#88cc88',
        vscSearch = '#cceeff',
        vscGray = '#999999',

        vscLightYellow = '#cccc44',
        vscYellow = '#994c4c',

    }
    local syntax = {}
    if vim.g.acme_style == "colorful" then
        syntax = {
            -- Syntax colors
            vscViolet = '#8888cc',
            vscBlue = '#0088cc',
            vscDarkBlue = '#000088',
            vscLightBlue = '#004488',
            vscGreen = '#448844',
            vscBlueGreen = '#005555',
            vscLightGreen = '#008800',
            vscRed = '#660000',
            vscOrange = '#777700',
            vscLightRed = '#bb5d5d',
            vscYellowOrange = '#dd9344',
            vscPink = '#660066',
            vscUiBlue = '#00aaff',
        }
    else
        syntax = {
            -- Syntax colors
            vscViolet = '#000000',
            vscBlue = '#000000',
            vscDarkBlue = '#000000',
            vscLightBlue = '#000000',
            vscGreen = '#000000',
            vscBlueGreen = '#000000',
            vscLightGreen = '#000000',
            vscRed = '#000000',
            vscOrange = '#000000',
            vscLightRed = '#000000',
            vscYellowOrange = '#000000',
            vscLightYellow = '#000000',
            vscYellow = '#000000',
            vscPink = '#000000',
            vscUiBlue = '#000000',
        }
    end

    for k,v in pairs(syntax) do colors[k] = v end


    return colors
end

return {generate = generate}
