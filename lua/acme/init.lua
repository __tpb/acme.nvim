-- acme.nvim color scheme
-- Lua port of https://github.com/tomasiser/vim-code-dark
-- By http://github.com/mofiqul

local utils = require("acme.utils")
local acme = {}


acme.set = function ()
    utils.load()
end

acme.change_style = function (style)
    vim.g.acme_style = style
    print("Acme style: ", style)
    vim.cmd[[colorscheme acme]]
end

return acme
