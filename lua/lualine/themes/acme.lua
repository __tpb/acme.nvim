-- Copyright (c) 2020-2021 Mofiqul Islam
-- MIT license, see LICENSE for more details.
local acme = {}

local colors = {
  black        = "#000000",
  white        = '#ffffea',
  gray         = '#eaeaea',
  darkgray     = '#999999',
  red          = '#ffeaea',
  green        = '#55aa55',
  blue         = '#9eeeee',
  lightblue    = '#eaffff',
  yellow       = '#cccc88',
  violet       = '#8888cc',
}

acme.normal = {
  b = {fg = colors.darkgray, bg = colors.gray},
  a = {fg = colors.black, bg = colors.lightblue, gui = 'bold'},
  c = {fg = colors.black, bg = colors.gray}
}

acme.visual = {
  a = {fg = colors.black, bg = colors.lightblue, gui = 'bold'},
  b = {fg = colors.black, bg = colors.violet},
}

acme.inactive = {
  b = {fg = colors.darkgray, bg = colors.white},
  a = {fg = colors.darkgray, bg = colors.gray, gui = 'bold'}
}

acme.replace = {
  b = {fg = colors.black, bg = colors.yellow},
  a = {fg = colors.black, bg = colors.yellow, gui = 'bold'},
  c = {fg = colors.black, bg = colors.gray}
}

acme.insert = {
  b = {fg = colors.darkgray, bg = colors.gray},
  a = {fg = colors.black, bg = colors.blue, gui = 'bold'},
  c = {fg = colors.black, bg = colors.gray}
}

return acme
