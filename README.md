#### Note: I'm planning on incorporating my current themes into a theme pack. I'll probably not be actively making changes to the current themes while that's underway

# acme.nvim

Acme.nvim is a lua based colorscheme inspired by [Parchment](ajgrf/parchment), the Acme editor, and the [color palette](https://cs.opensource.google/go/go/+/refs/tags/go1.17:src/image/color/palette/palette.go) used by Plan9 from Bell Labs.

This theme should be considered in beta condition. Expect bugs, refactorings, and potential changes
to the theme's look and feel.

## Screenshots
No syntax:
![Plain](./plain.png)
With syntax:
![Colorful](./colorful.png)

## #️ Supported Plugins
- [LSP](https://github.com/neovim/nvim-lspconfig)
- [Treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
- [nvim-compe](https://github.com/hrsh7th/nvim-compe)
- [Telescope](https://github.com/nvim-telescope/telescope.nvim)
- [Neogit](https://github.com/TimUntersberger/neogit)
- [NvimTree](https://github.com/kyazdani42/nvim-tree.lua)
- [BufferLine](https://github.com/akinsho/nvim-bufferline.lua)
- [Git Signs](https://github.com/lewis6991/gitsigns.nvim)
- [Git Gutter](https://github.com/airblade/vim-gitgutter)
- [Lualine](https://github.com/hoob3rt/lualine.nvim)
- [Indent Blankline](https://github.com/lukas-reineke/indent-blankline.nvim)


## ⬇️ Installation

Install via package manager

```lua
-- Using Packer:
use 'https://gitlab.com/__tpb/acme.nvim'
```

## 🚀 Usage

```lua
-- Lua:
-- For colorful syntax
vim.g.acme_style = "colorful"
vim.cmd[[colorscheme acme]]

```
```vim
" Vim-Script:
"For colorful syntax
let g:acme_style = "colorful"
colorscheme acme
```

If you are using [`lualine`](https://github.com/hoob3rt/lualine.nvim), you can also enable the provided theme:


```lua
require('lualine').setup {
options = {
-- ... 
theme = 'acme'
-- ... 
}
}'
```

[nvim-bufferline.lua](https://github.com/akinsho/nvim-bufferline.lua)  setup for exact match as screen shots

```lua
-- Buffer line setup

require'bufferline'.setup{
options = {
indicator_icon = ' ',
buffer_close_icon = '',
modified_icon = '●',
close_icon = '',
close_command = "Bdelete %d",
right_mouse_command = "Bdelete! %d",
left_trunc_marker = '',
right_trunc_marker = '',
offsets = {{filetype = "NvimTree", text = "EXPLORER", text_align = "left"}},
show_tab_indicators = true,
show_close_icon = false
},
highlights = {
fill = {
guifg = {attribute = "fg", highlight = "Normal"},
guibg = {attribute = "bg", highlight = "StatusLineNC"},
},
background = {
guifg = {attribute = "fg", highlight = "Normal"},
guibg = {attribute = "bg", highlight = "SignColumn"}
},
buffer_visible = {
gui = "",
guifg = {attribute = "fg", highlight="Normal"},
guibg = {attribute = "bg", highlight = "Normal"}
},
buffer_selected = {
gui = "",
guifg = {attribute = "fg", highlight="Normal"},
guibg = {attribute = "bg", highlight = "StatusLine"}
},
separator = {
guifg = {attribute = "bg", highlight = "Normal"},
guibg = {attribute = "bg", highlight = "SignColumn"},
},
separator_selected = {
guifg = {attribute = "fg", highlight="Special"},
guibg = {attribute = "bg", highlight = "StatusLine"}
},
separator_visible = {
guifg = {attribute = "fg", highlight = "Normal"},
guibg = {attribute = "bg", highlight = "StatusLineNC"},
},
close_button = {
guifg = {attribute = "fg", highlight = "Normal"},
guibg = {attribute = "bg", highlight = "SignColumn"}
},
close_button_selected = {
guifg = {attribute = "fg", highlight="normal"},
guibg = {attribute = "bg", highlight = "StatusLine"}
},
close_button_visible = {
guifg = {attribute = "fg", highlight="normal"},
guibg = {attribute = "bg", highlight = "normal"}
},

}
```

## Switching theme 
```
:lua require('acme').change_style("colorful") 
:lua require('acme').change_style("plain") 
```

### Something is broken but I know how to fix it!
Pull requests are welcome! Feel free to send one with an explanation!
